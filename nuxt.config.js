export default {
  mode: 'universal',

  env: {
    baseUrl: 'https://cockpit.santihans.com',
  },
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        type: 'text/css',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons',
      },
    ],
  },

  meta: {
    name: 'Olivier Christe',
    description: 'Olivier Christe - Journalist, Photograph',
    theme_color: '#000000',
    ogHost: 'https://www.olivierchriste.ch',
    ogImage: { path: '/og-image.png' },
    twitterCard: 'summary',
    twitterCreator: '@santhans4056',
  },

  manifest: {
    name: 'Olivier Christe',
    short_name: 'Olivier Christe',
    description: 'Olivier Christe - Journalist, Photograph',
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: ['@nuxtjs/pwa'],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },

  generate: {
    dir: 'dist',
  },
}
